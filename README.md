# What will the value of the Wilshire 5000 be in 7 years?  

We pull all daily, monthly, annual frequency series from the FRED database that cover the period from 1971-1-1 to the present and use a multi layer perceptron to predict the Wilshire 5000 value 7 years from the present. <br/>
<br/> 
This approach yields 5,656 features with daily frequency. FRED boasts of 527,000 data series. We find 7,107 series that cover the entire period of the Wilshire 5000 series,7,017 are sucessfully pulled, and 5,656 series that don't have gaps in data larger than 365 days. All non daily series are resampled at daily frequency and forward filled up to a maximum of 365 days.  
<br/>
We consider three different data sets: <br/> 1) from 1971-1-1 to 1993-3-24 (targets up to peak of tech bubble)<br/>
                                             2) from 1971-1-1 to 2000-10-9 (targets up to peak of housing bubble)<br/>
                                             3) from 1971-1-1 to 2011-9-16 (targets up to present of everything bubble)<br/>
<br/>                                        
We use the Sci Kit Learn MLP Regressor with early stopping against a validation subset of 25% within each train set. ADAM is used for descent and rectified linear units for the activation function. Good performance is achieved with 13 hidden layers with number of nodes in each layer equal to 1/3 the number of features pulled. The data is dense. Alpha and initial learning rate are set to 1/10,000. 

<br/>
The period beyond train sets 1 and 2 are the test sets. Neither trained, nor validated on. 
Train set 3 does not have a test set.
<br/>
We plot a graph of our predictions for each training method. <br/>
<br/>
We bag the MLP Regressor on Random Patches to reduce variance. 






